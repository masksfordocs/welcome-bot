const WelcomeMessage = ':wave: Hi there! Welcome to MasksForDocs! If you haven\'t joined them yet, we\'d encourage you to check out the following channels:'

const InterestMap = {
    // Driving
    "a car": "#logistics-coordination",
    "\\Wdeliver\\W": "#logistics-coordination",
    "motorcycle": "#moto,#logistics-coordination",
    "\\Wbike\\W": "#moto,#logistics-coordination",
    "logistics": "#logistics-coordination",
    "coordination": "#logistics-coordination",

    // 3d printing
    "\\W3d\\W": "#skill-makers,#skill-3d-printing",
    "\\Wprint\\W": "#skill-makers,#skill-3d-printing",
    "\\Wprinter\\W": "#skill-makers,#skill-3d-printing",
    "\\Wprinting\\W": "#skill-makers,#skill-3d-printing",
    "3DVerkstan": "#skill-makers,#skill-3d-printing",
    "prusa": "#skill-makers,#skill-3d-printing",
    "ender": "#skill-makers,#skill-3d-printing",
    "\\WPLA\\W": "#skill-makers,#skill-3d-printing",
    "\\WPETG\\W": "#skill-makers,#skill-3d-printing",
    "cr\-?10": "#skill-makers,#skill-3d-printing",
    "lulzbot": "#skill-makers,#skill-3d-printing",
    "davinci": "#skill-makers,#skill-3d-printing",
    "flashforge": "#skill-makers,#skill-3d-printing",
    "glowforge": "#skill-makers,#skill-3d-printing",
    "anycubic": "#skill-makers,#skill-3d-printing",
    "maker": "#skill-makers",

    // Sewing
    "\\Wsew\\W": "#skill-makers,#skill-sewing",
    "\\Wsewing\\W": "#skill-makers,#skill-sewing",
    "\\Wfabric\\W": "#skill-makers,#skill-sewing",
    "homemade masks": "#skill-makers,#skill-sewing",

    // Developing
    "\\Wcode\\W": "#developers",
    "\\Wcoder\\W": "#developers",
    "\\Wcoding\\W": "#developers",
    "\\Wdevelop\\W": "#developers",
    "\\Wdeveloper\\W": "#developers",
    "\\Wdeveloping\\W": "#developers",
    "angular": "#developers",
    "aws": "#developers",
    "azure": "#developers",
    "back end": "#developers",
    "devops": "#developers",
    "docker": "#developers",
    "CI/CD": "#developers",
    "cloud": "#developers",
    "cyber": "#developers",
    "elixir": "#developers",
    "erlang": "#developers",
    "gcp": "#developers",
    "gitlab": "#developers",
    "javascript": "#developers",
    "phoenix": "#developers",
    "postgres": "#developers",
    "react": "#developers"
}

const LocationMap = {
    // Arizona
    "arizona": "#zlocal-us-az",
    "\\WAZ\\W": "#zlocal-us-az",

    // Canada
    "toronto": "#zlocal-ca-toronto",

    // California
    "san diego": "#zlocal-us-ca-sd,#zlocal-us-ca",
    "san francisco": "#zlocal-us-ca-sfo,#zlocal-us-ca",
    "bay area": "#zlocal-us-ca-sfo,#zlocal-us-ca",
    "\\WSF\\W": "#zlocal-us-ca-sfo,#zlocal-us-ca",

    "los angeles": "#zlocal-us-ca-lax,#zlocal-us-ca",
    "\\WLA\\W": "#zlocal-us-ca-lax,#zlocal-us-ca",
    "\\WCA\\W": "#zlocal-us-ca",
    "california": "#zlocal-us-ca",

    "oakland": "#zlocal-us-ca-oak,#zlocal-us-ca",
    "\\WOAK\\W": "#zlocal-us-ca-oak,#zlocal-us-ca",

    // Colorado
    "denver": "#zlocal-us-co-den",
    "colorado": "#zlocal-us-co-den",
    "\\WCO\\W": "#zlocal-us-co-den",

    // Florida
    "miami": "#zlocal-us-fl-mia,#zlocal-us-fl",
    "tampa": "#zlocal-us-fl-tpa,#zlocal-us-fl",
    "\\WFL\\W": "#zlocal-us-fl",
    "florida": "#zlocal-us-fl",

    // Georgia
    "atlanta": "#zlocal-us-ga-atl",
    "savannah": "#zlocal-us-ga-atl",
    "\\WGA\\W": "#zlocal-us-ga-atl",

    // Illinois
    "chicago": "#zlocal-us-il-chi",
    "illinois": "#zlocal-us-il-chi",
    "\\WIL\\W": "#zlocal-us-il-chi",

    // Louisiana
    "lousiana": "#zlocal-us-la",
    "new orleans": "#zlocal-us-la-no,#zlocal-us-la",

    // Massachusetts
    "boston": "#zlocal-us-ma",
    "massachusetts": "#zlocal-us-ma",
    "\\WMA\\W": "#zlocal-us-ma",

    // Michigan
    "michigan": "#zlocal-us-mi",
    "\\WMI\\W": "#zlocal-us-mi",
    "detroit": "#zlocal-us-mi-dtw,#zlocal-us-mi",
    "ann arbor": "#zlocal-us-mi-arb,#zlocal-us-mi",

    // Minneapolis
    "minneapolis": "#zlocal-us-mn-msp",
    "minnesota": "#zlocal-us-mn-msp",

    // Nebraska
    "omaha": "#zlocal-us-ne-oma",
    "nebraska": "#zlocal-us-ne-oma",

    // Nevada
    "nevada": "#zlocal-us-nv",
    "las vegas": "#zlocal-us-nv-lv,#zlocal-us-nv",
    "\\WNV\\W": "#zlocal-us-nv-lv,#zlocal-us-nv",

    // New Jersey
    "\\WNJ\\W": "#zlocal-us-nj",

    // New Mexico
    "new mexico": "#zlocal-us-nm",

    // New York and NYC
    "new york city": "#zlocal-us-ny-newyorkcity,#zlocal-us-ny",
    "Rochester": "#zlocal-us-ny-western-ny,#zlocal-us-ny",
    "\\Wnyc\\W": "#zlocal-us-ny-newyorkcity,#zlocal-us-ny",
    "manhattan": "#zlocal-us-ny-newyorkcity,#zlocal-us-ny",
    "new york": "#zlocal-us-ny",
    "[\\W]NY": "#zlocal-us-ny",

    // North Carolina
    "north carolina": "#zlocal-us-nc",
    "[\\W]NC": "#zlocal-us-nc",

    // Oklahoma
    "oklahoma": "#zlocal-us-ok",
    "\\WOKC\\W": "#zlocal-us-ok",

    // Oregon
    "oregon": "#zlocal-us-or",
    "portland": "#zlocal-us-or-pdx,#zlocal-us-or",
    "eugene": "#zlocal-us-or-eug,#zlocal-us-org",

    // Texas
    "austin\\W": "#zlocal-us-tx-atx,#zlocal-us-tx",
    "san antonio": "#zlocal-us-tx-sat,#zlocal-us-tx",
    "dallas": "#zlocal-us-tx-dfw,#zlocal-us-tx",
    "ft.? worth": "#zlocal-us-tx-dfw,#zlocal-us-tx",
    "plano": "#zlocal-us-tx-dfw,#zlocal-us-tx",
    "denton": "#zlocal-us-tx-dfw,#zlocal-us-tx",
    "[\\W]DFW": "#zlocal-us-tx-dfw,#zlocal-us-tx",
    "houston": "#zlocal-us-tx-hou,#zlocal-us-tx",
    "[\\W]TX": "#zlocal-us-tx",
    "texas": "#zlocal-us-tx",

    // Utah
    "utah": "#zlocal-us-ut",
    "salt lake city": "#zlocal-us-ut-slc,#zlocal-us-ut",

    // Virginia
    "northern virginia": "#zlocal-us-va-nova",

    // Washington
    "seattle": "#zlocal-us-wa",
    "washington": "#zlocal-us-wa",

    // Wisconson
    "\\WWI\\W": "#zlocal-us-wi"
}

exports.WelcomeMessage = WelcomeMessage
exports.InterestMap = InterestMap
exports.LocationMap = LocationMap
