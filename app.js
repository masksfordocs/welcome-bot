const { App } = require('@slack/bolt');
const { InterestMap, LocationMap, WelcomeMessage } = require('./messages')

const IntroductionChannelId = 'C010F8YM10U'
const DevChannelId = 'G010DU405GS'

function channelFilter({ message, next }) {
    if ((message.channel != IntroductionChannelId) && (message.channel != DevChannelId)) {
        return
    }
    if (message.thread_ts !== undefined) {
        return
    }
    next();
}

function matcher(text, mapping, recommendations) {
    for (let pattern in mapping) {
        regex = new RegExp(pattern, 'im')
        recommendedChannels = mapping[pattern]
        if (regex.test(text)) {
            recommendedChannels.split(",").map(channel => {
                if (!recommendations.includes(channel)) {
                    recommendations.push(channel)
                }
            })
        }
    }
    return recommendations
}

// Initializes your app with your bot token and signing secret
const app = new App({
  token: process.env.SLACK_BOT_TOKEN,
  signingSecret: process.env.SLACK_SIGNING_SECRET
});

app.message(channelFilter, async ({ message, context }) => {
    // First, check for local channels since those are the most important
    recommendedChannels = matcher(message.text, LocationMap, [])
    recommendedChannels = matcher(message.text, InterestMap, recommendedChannels)

    // If we didn't match anything, short-circuit
    if (!recommendedChannels.length) {
        console.log("No recommendations")
        return
    }

    response = WelcomeMessage + '\n'
    recommendedChannels.map((channel) => {
        response += `\n${channel}`
    })
    console.log(message)
    console.log("Sending Response: " + response)

    try {
      // Call the chat.scheduleMessage method with a token
      const result = await app.client.chat.postMessage({
        // The token you used to initialize your app is stored in the `context` object
        token: context.botToken,
        channel: message.channel,
        thread_ts: message.ts,
        link_names: true,
        text: response
      });
    }
    catch (error) {
      console.error(error);
    }
  });

(async () => {
  // Start your app
  await app.start(process.env.PORT || 3000);

  console.log('⚡️ Bolt app is running!');
})();
